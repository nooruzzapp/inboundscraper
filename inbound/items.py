# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class InboundItem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()
    url = scrapy.Field()
    karma = scrapy.Field()
    twitter = scrapy.Field()
    website = scrapy.Field()
    linkedin = scrapy.Field()
    tag = scrapy.Field()