import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from inbound.items import InboundItem
from inbound.spiders.db import connection
from lxml import html

class InboundScraper(CrawlSpider):
    name = 'inbound'
    allowed_domains = ['inbound.org']
    start_urls = ['https://inbound.org/members/all/hot?&per_page=%s' %page for page in range(0, 96, 48)]


    def parse(self, response):
         
        tree = html.fromstring(response.body)
        for tree in tree.xpath("//div[@class='panel-body']/a/@href"):
            yield scrapy.Request(tree, callback=self.parse_item)

    def parse_item(self, response):
        tree = html.fromstring(response.body)
        for sel in tree:
            item = InboundItem()
            item['name'] = sel.xpath("//div['member-details-container']/h1//text()")[0]
            item['url'] = response.url
            item['karma'] = sel.xpath("//span[@class='karma']//text()")[0]
            item['twitter'] = sel.xpath("//div['social-links']/a[@class='twitter']/@href")[0]
            item['website'] = sel.xpath("//html/body/div/div/div/div/div/div[1]/div/a[not(contains(@href,'google')) and not(contains(@href,'linkedin')) and not(contains(@href,'inbound')) and not(contains(@href,'facebook')) and not(contains(@href,'twitter'))]/@href")[0]
            item['linkedin'] = sel.xpath("//div['social-links']/a[@class='linkedin']/@href")[0]
            item['tag'] = sel.xpath("//div[@class='member-banner-tagline']/p//text()")[0] + sel.xpath("//div[@class='member-banner-tagline']/p/a//text()")[0]
            yield item
            print (item)