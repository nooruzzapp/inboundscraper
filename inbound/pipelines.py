# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import psycopg2
from inbound.items import *

class InboundPipeline(object):
    def __init__(self):
        self.connection = psycopg2.connect(host='localhost', database='inbound', user='postgres', password='postgres')
        self.cursor = self.connection.cursor()  

    def process_item(self, item, spider):
        try:
            if type(item) is InboundItem:
                self.cursor.execute("""INSERT INTO data (name, url, karma, twitter, website, linkedin, tag) VALUES (%s, %s, %s, %s, %s, %s, %s)""", (item.get('name'), item.get('url'), item.get('karma'), item.get('twitter'), item.get('website'), item.get('linkedin'), item.get('tag'),))
                self.connection.commit()
 
        except e:
            self.connection.rollback()
            print ("Error: %s" % e)
        return item
